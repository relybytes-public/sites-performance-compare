# sitecomparestats

![screenshot](console.png)

sitecomparestats visualizes `curl` statistics (as same as httpstats) for a list of urls.
Create also a csv for compare the results

![csv_screen_visual_studio_code](csv_screen_visual_studio_code.png)

Pro:

- is a python script
- without dependencies
- compatible with python 3
- csv standard (without complex dependencies)

## Installation

Download the script directly from gitlab repository

## Usage

Simply:

```bash
python sitecomparestats.py www.google.com
```

```bash
python sitecomparestats.py www.google.com,www.facebook.com
```

```bash
python sitecomparestats.py www.mydomain/page1,www.mydomain/page2
```

### cURL Options

Because `sitecomparestats` is a wrapper of cURL, you can pass any cURL supported option after the url (except for `-w`, `-D`, `-o`, `-s`, `-S` which are already used by `sitecomparestats`):

```bash
sitecomparestats httpbin.org/post -X POST --data-urlencode "a=b" -v
```

### Environment Variables

`sitecomparestats` has a bunch of environment variables to control its behavior.
Here are some usage demos, you can also run `sitecomparestats --help` to see full explanation.

- <strong><code>SITECOMPARESTAT_SHOW_BODY</code></strong>

  Set to `true` to show response body in the output, note that body length
  is limited to 1023 bytes, will be truncated if exceeds. Default is `false`.

- <strong><code>SITECOMPARESTAT_SHOW_IP</code></strong>

  By default sitecomparestats shows remote and local IP/port address.
  Set to `false` to disable this feature. Default is `true`.

- <strong><code>SITECOMPARESTAT_SHOW_SPEED</code></strong>

  Set to `true` to show download and upload speed. Default is `false`.

  ```bash
  SITECOMPARESTAT_SHOW_SPEED=true sitecomparestats http://cachefly.cachefly.net/10mb.test

  ...
  speed_download: 3193.3 KiB/s, speed_upload: 0.0 KiB/s
  ```

- <strong><code>SITECOMPARESTAT_SAVE_BODY</code></strong>

  By default sitecomparestats stores body in a tmp file,
  set to `false` to disable this feature. Default is `true`

- <strong><code>SITECOMPARESTAT_CURL_BIN</code></strong>

  Indicate the cURL bin path to use. Default is `curl` from current shell $PATH.

  This exampe uses brew installed cURL to make HTTP2 request:

  ```bash
  SITECOMPARESTAT_CURL_BIN=/usr/local/Cellar/curl/7.50.3/bin/curl sitecomparestats https://http2.akamai.com/ --http2

  HTTP/2 200
  ...
  ```

  > cURL must be compiled with nghttp2 to enable http2 feature
  > ([#12](https://github.com/reorx/sitecomparestats/issues/12)).

- <strong><code>SITECOMPARESTAT_METRICS_ONLY</code></strong>

  If set to `true`, sitecomparestats will only output metrics in json format,
  this is useful if you want to parse the data instead of reading it.

- <strong><code>SITECOMPARESTAT_DEBUG</code></strong>

  Set to `true` to see debugging logs. Default is `false`

For convenience, you can export these environments in your `.zshrc` or `.bashrc`,
example:

```bash
export SITECOMPARESTAT_SHOW_IP=false
export SITECOMPARESTAT_SHOW_SPEED=true
export SITECOMPARESTAT_SAVE_BODY=false
```
